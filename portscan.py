# Made by Anthony Cuenca
from socket import *
import threading
import time 
#==================================================================================
def keyCheck(key,dict):
    if key in dict:
        return True
    else:
        return False 
#==================================================================================
def TCPConnect(ip, portNum, delay, output):
    #scan reserved ports
    s = socket(AF_INET, SOCK_STREAM)
    result = s.connect_ex((ip, portNum))
    s.settimeout(delay)

    if(result == 0) :
        output[portNum] = 'Listening'
    s.close()
        
#==================================================================================
def startScan(host_ip, delay):

    threads = []        # To run TCP_connect concurrently
    output = {}         # For printing purposes

    # Spawning threads to scan ports
    for i in range(20, 1025):
        t = threading.Thread(target=TCPConnect, args=(host_ip, i, delay, output))
        threads.append(t) #Starts from index 0 and then appends each thread to the list

    # Starting threads
    for i in range(len(threads)):
        time.sleep(0.1)
        threads[i].start()
        
    # Locking the script until all threads complete
    for i in range(len(threads)):
        threads[i].join()

    # Printing listening ports from small to large
    for i in range(len(threads)):
        if (keyCheck(i,output) == True):
            if (output[i] == 'Listening'):
                print(str(i) + ': ' + output[i])
            
#==================================================================================
print("Enter the IP address (URL works too)")        
host_ip = gethostbyname(input())

print("How many seconds the socket is going to wait until timeout (Keep in mind distance of target!): ")
delay = int(input())

print("Scanning for open ports! The process will take approximately a few minutes...")
startScan(host_ip, delay)
